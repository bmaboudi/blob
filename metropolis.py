import numpy as np
import high_fidelity as hf
import progressbar
import torch

class Metropolis:
	def __init__(self, y_observed, sigma):
		self.y_observed = y_observed
		self.sigma2 = sigma*sigma
		self.epsilon = 2.22044604925e-16
		self.N = 1000000

		self.hf = hf.high_fidelity(num_out=10, num_modes=50)
		self.N_vertex = 4
		self.x0 = self.find_initial_state()
		self.X = []

	def sort_vectors(self,x,y):
		alpha = np.zeros(x.shape[0])
		for i in range(alpha.size):
			if(x[i]>0 and y[i]>0):
				alpha[i] = np.arctan(y[i]/x[i])
			elif(x[i]<0 and y[i]>0):
				alpha[i] = np.pi + np.arctan(y[i]/x[i])
			elif(x[i]<0 and y[i]<0):
				alpha[i] = np.pi + np.arctan(y[i]/x[i])
			else:
				alpha[i] = 2*np.pi + np.arctan(y[i]/x[i])

		return np.argsort(alpha)

	def find_initial_state(self):
		x = np.random.uniform(0,1,self.N_vertex-2)

		idx = np.random.permutation(self.N_vertex-2)
		cut_off = np.random.randint(self.N_vertex-3) + 1

		temp = np.sort(x[idx[0:cut_off]])
		x1 = np.zeros(temp.size+1)
		x1[0] = temp[0]
		for i in range(1,temp.size):
			x1[i] = temp[i]-temp[i-1]
		x1[-1] = 1 - temp[-1]

		temp = np.sort(-x[idx[cut_off:]])
		x2 = np.zeros(temp.size+1)
		x2[0] = -1 - temp[0]
		for i in range(1,temp.size):
			x2[i] = temp[i-1]-temp[i]
		x2[-1] = temp[-1]

		y = np.random.uniform(0,1,self.N_vertex-2)

		idx = np.random.permutation(self.N_vertex-2)
		cut_off = np.random.randint(self.N_vertex-3) + 1

		temp = np.sort(y[idx[0:cut_off]])
		y1 = np.zeros(temp.size+1)
		y1[0] = temp[0]
		for i in range(1,temp.size):
			y1[i] = temp[i]-temp[i-1]
		y1[-1] = 1 - temp[-1]

		temp = np.sort(-y[idx[cut_off:]])
		y2 = np.zeros(temp.size+1)
		y2[0] = -1 - temp[0]
		for i in range(1,temp.size):
			y2[i] = temp[i-1]-temp[i]
		y2[-1] = temp[-1]

		x = np.concatenate([x1,x2],axis=0)
		y = np.concatenate([y1,y2],axis=0)

		idx = np.random.permutation(x.size)
		x = x[idx]
		idx = np.random.permutation(y.size)
		y = y[idx]

		cos_ang = y/np.sqrt(x**2+y**2)
		idx = self.sort_vectors(x,y)
		#idx = np.argsort(alpha)
		x = x[idx]
		y = y[idx]

		p = np.zeros([self.N_vertex,2])
		for i in range(0,self.N_vertex-1):
			p[i+1,0] = p[i,0] + x[i]
			p[i+1,1] = p[i,1] + y[i]


		av = np.sum(p,axis = 0)/self.N_vertex
		p = p - av

		x = np.zeros(8)
		x[0:4] = p[:,0]
		x[4:8] = p[:,1]
		#x[3:103] = np.random.normal(0,1,100)
		return x

	#update conjugation
	def conjugate_initial_state(self,x):
		y = np.zeros(3)
		y[0] = -x[0]
		y[1] = -x[1]
		y[2] = 0.55 - x[2]
		y[3] =  10 - x[3]
		return y

	def set_eval_func(self, eval_func):
		self.eval_func = eval_func


	def density_ratio(self, x, y):
		#modes = np.linspace(1,9,9)
		fx = []
		fy = []
		#for mode in modes:
		#	x[-1] = mode
		#	y[-1] = mode
		torch_x = torch.from_numpy( np.reshape(x,[1,-1]) ).float()
		torch_y = torch.from_numpy( np.reshape(y,[1,-1]) ).float()
		#	fx.append( self.eval_func(torch_x) )
		#	fy.append( self.eval_func(torch_y) )
		fx = self.eval_func(torch_x)
		fy = self.eval_func(torch_y)
			#ex = np.exp( - np.inner(fx - self.y_observed, fx - self.y_observed)/2/self.sigma2)
			#ey = np.exp( - np.inner(fy - self.y_observed, fy - self.y_observed)/2/self.sigma2)
		#fx = np.reshape(fx,[-1])
		#fy = np.reshape(fy,[-1])

		exponent = - np.inner(fy - self.y_observed, fy - self.y_observed) + np.inner(fx - self.y_observed, fx - self.y_observed)

		if(exponent/2/self.sigma2 > 1):
			ratio = 2.0
		else:
			ratio = np.exp(exponent/2/self.sigma2)
		return np.min([1.0, ratio])

	def is_covex(self,x,y):
		vx = np.zeros(5)
		vy = np.zeros(5)
		vx[0] = x[1] - x[0]
		vx[1] = x[2] - x[1]
		vx[2] = x[3] - x[2]
		vx[3] = x[0] - x[3]
		vx[4] = vx[0]

		vy[0] = y[1] - y[0]
		vy[1] = y[2] - y[1]
		vy[2] = y[3] - y[2]
		vy[3] = y[0] - y[3]
		vy[4] = vy[0]

		s = np.sign( vx[0]*vy[1] - vy[0]*vx[1] )
		convexity = True
		for i in range(1,4):
			if np.sign( vx[i]*vy[i+1] - vy[i]*vx[i+1] ) != s:
				convexity = False
				break

		return convexity

	def checker(self, x):
		check = True
		for i in range(x.shape[0]):
			if(x[i] < -1 or x[i] > 1):
				check = False
		if( self.is_covex(x[0:self.N_vertex],x[self.N_vertex:]) == False ):
			check = False
		return check

	def make_av_zero(self, x):
		x[0:4] = x[0:4] - sum(x[0:4])/4
		x[4:8] = x[4:8] - sum(x[4:8])/4
		return x

	def run_metro_conj(self):
		self.metro_alg()
		self.x0 = self.conjugate_initial_state(self.x0)
		print(self.compute_mean())
		self.metro_alg()

	def metro_multi_seed(self,num_seeds):
		mean = []
		var = []
		for i in range(num_seeds):
			print('iteration {} out of {}'.format(i+1,num_seeds))
			self.x0 = self.find_initial_state()
			self.metro_alg()
			mean.append( self.compute_mean() )
			var.append( self.compute_var() )
			print(self.compute_mean())
			print(self.compute_var())
			self.X.clear()

		mean = np.reshape(mean,[num_seeds,-1])
		print( np.sum(mean,axis=0)/num_seeds )
		return mean


	def metro_alg(self):
		x = self.x0
		self.X.append(x)

#		for i in range(0,self.N):
		for i in progressbar.progressbar(range(0, self.N)):
			y = np.random.uniform(0,1,8)
			y = x + 0.05*( y - 0.5 )
			y = self.make_av_zero(y)
			self.make_av_zero(y)
			if( self.checker(y) ):
				alpha = self.density_ratio(x, y)
				u = np.random.uniform(0,1)
				if( u < alpha ):
					x = y
					self.X.append(y)
				else:
					self.X.append(x)
			else:
				continue

	def compute_mean(self):
		N = len(self.X)
		samples = np.reshape( self.X, [N,-1] )
		mean = np.sum(samples,axis = 0)/N
		return mean

	def compute_var(self):
		N = len(self.X)
		mean = self.compute_mean()
		samples = np.reshape( self.X, [N,-1] )
		samples = samples - mean
		np.set_printoptions(threshold=np.inf)
		cov = np.matmul( samples.T, samples ) / N

		return np.sqrt(np.diagonal(cov))
