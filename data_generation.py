import numpy as np
import high_fidelity as hf
import torch
import progressbar

def sort_vectors(x,y):
	alpha = np.zeros(x.shape[0])
	for i in range(alpha.size):
		if(x[i]>0 and y[i]>0):
			alpha[i] = np.arctan(y[i]/x[i])
		elif(x[i]<0 and y[i]>0):
			alpha[i] = np.pi + np.arctan(y[i]/x[i])
		elif(x[i]<0 and y[i]<0):
			alpha[i] = np.pi + np.arctan(y[i]/x[i])
		else:
			alpha[i] = 2*np.pi + np.arctan(y[i]/x[i])

	return np.argsort(alpha)

def conv_pol(N,model_params):
	x = np.random.uniform(0,1,N-2)

	idx = np.random.permutation(N-2)
	cut_off = np.random.randint(N-3) + 1

	temp = np.sort(x[idx[0:cut_off]])
	x1 = np.zeros(temp.size+1)
	x1[0] = temp[0]
	for i in range(1,temp.size):
		x1[i] = temp[i]-temp[i-1]
	x1[-1] = 1 - temp[-1]

	temp = np.sort(-x[idx[cut_off:]])
	x2 = np.zeros(temp.size+1)
	x2[0] = -1 - temp[0]
	for i in range(1,temp.size):
		x2[i] = temp[i-1]-temp[i]
	x2[-1] = temp[-1]

	y = np.random.uniform(0,1,N-2)

	idx = np.random.permutation(N-2)
	cut_off = np.random.randint(N-3) + 1

	temp = np.sort(y[idx[0:cut_off]])
	y1 = np.zeros(temp.size+1)
	y1[0] = temp[0]
	for i in range(1,temp.size):
		y1[i] = temp[i]-temp[i-1]
	y1[-1] = 1 - temp[-1]

	temp = np.sort(-y[idx[cut_off:]])
	y2 = np.zeros(temp.size+1)
	y2[0] = -1 - temp[0]
	for i in range(1,temp.size):
		y2[i] = temp[i-1]-temp[i]
	y2[-1] = temp[-1]

	x = np.concatenate([x1,x2],axis=0)
	y = np.concatenate([y1,y2],axis=0)

	idx = np.random.permutation(x.size)
	x = x[idx]
	idx = np.random.permutation(y.size)
	y = y[idx]

	cos_ang = y/np.sqrt(x**2+y**2)
	idx = sort_vectors(x,y)
	#idx = np.argsort(alpha)
	x = x[idx]
	y = y[idx]

	p = np.zeros([N,2])
	for i in range(0,N-1):
		p[i+1,0] = p[i,0] + x[i]
		p[i+1,1] = p[i,1] + y[i]


	av = np.sum(p,axis = 0)/N
	p = p - av
	model_params['px'] = p[:,0]
	model_params['py'] = p[:,1]
	return model_params

class data():
	def __init__(self,num_batches=0,batch_size=0,num_modes = 10,num_out=100,path=None):
		if(path == None):
			self.num_batches = num_batches
			self.batch_size = batch_size
			self.num_data = self.batch_size*self.num_batches

			self.in_data = list()
			self.out_data = list()

			self.torch_in = list()
			self.torch_out = list()

			self.high_fid = hf.high_fidelity(num_out=num_out)

		else:
			self.batch_size = batch_size

			data = np.load(path)
			self.raw_in = data['raw_in']
			self.raw_out = data['raw_out']
			num_data = self.raw_in.shape[0]
			self.num_batches = int(num_data/self.batch_size)
			self.num_data = self.num_batches*self.batch_size
			self.torch_in = list()
			self.torch_out = list()

			self.shuffle()


	def generate_model_params(self):
		model_params = {}
		model_params = conv_pol(4,model_params)
		return model_params

	def construct_param_vec(self,center,radius,height):
		model_params = {}
		model_params = conv_pol(4,model_params)
		input_vec = np.matrix( self.give_param_vec(model_params) )
		return torch.from_numpy( input_vec ).float()

	def give_param_vec(self, model_params):
		l = []
		for i in model_params:
			l.append( np.array( model_params[i] ) )

		return np.concatenate(l,axis=0)

	def new_batch(self):
		self.in_data.clear()
		self.out_data.clear()

		print('generating data...')
		for i in progressbar.progressbar(range(0,self.num_batches)):
#		for i in range(0,self.num_batches):
			local_in_data = []
			local_out_data = []
			for j in range(0,self.batch_size):
				model_params = self.generate_model_params()
				self.high_fid.set_params(model_params)
				self.high_fid.assemble_rhs()
				self.high_fid.assemble_bilinear_matrix()
				#out_data = self.high_fid.solve_multiple()
				out_data = self.high_fid.solve()
				in_data = self.give_param_vec(model_params)
				local_in_data.append( in_data )
				local_out_data.append( out_data )
			self.in_data.append( np.matrix( local_in_data ) )
			self.out_data.append( np.matrix( local_out_data ) )

	def new_batch_raw(self, path):
		raw_in = []
		raw_out = []
		#for i in range(self.num_data):
		for i in progressbar.progressbar(range(self.num_data)):
			model_params = self.generate_model_params()
			self.high_fid.set_params(model_params)
			self.high_fid.assemble_rhs()
			self.high_fid.assemble_bilinear_matrix()
			#out_data = self.high_fid.solve_multiple()
			out_data = self.high_fid.solve()
			in_data = self.give_param_vec(model_params)

			raw_in.append( in_data )
			raw_out.append( out_data )

		raw_in = np.reshape(raw_in,[self.num_data,-1])
		raw_out = np.reshape(raw_out,[self.num_data,-1])

		np.savez(path,raw_in=raw_in, raw_out=raw_out)

	def shuffle(self):
		self.torch_in.clear()
		self.torch_out.clear()

		rand_list = np.random.permutation( self.num_data )
		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				idx = rand_list[i*self.batch_size + j]
				local_in.append(self.raw_in[idx])
				local_out.append(self.raw_out[idx])
			local_in = np.reshape(local_in,[self.batch_size,-1])
			local_out = np.reshape(local_out,[self.batch_size,-1])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

	def generate_train_data(self):
		self.new_batch()
		filename = 'train_data.npz'
		np.savez(filename, in_data=self.in_data, out_data=self.out_data)

	def generate_test_data(self):
		self.new_batch()
		filename = 'test_data.npz'
		np.savez(filename, in_data=self.in_data, out_data=self.out_data)

	def update_data(self, sigma):
		rm_idx = np.random.permutation( self.num_batches )
		rm_idx = rm_idx[0:int( sigma*self.num_batches ) ]

#        for idx in rm_idx:
		print('generating data...')
		for idx in progressbar.progressbar(rm_idx):
			local_in_data = []
			local_out_data = []
			for j in range(0,self.batch_size):
				model_params = self.generate_model_params()
				self.high_fid.set_params(model_params)
				self.high_fid.assemble_bilinear_matrix()
				out_data = self.high_fid.solve_multiple()
				in_data = self.give_param_vec(model_params)
				local_in_data.append( in_data )
				local_out_data.append( out_data )

			self.in_data[idx,:,:] = local_in_data
			self.out_data[idx,:,:] = local_out_data

		self.to_troch()

	def save_data(self,filename):
		np.savez(filename, in_data=self.in_data, out_data=self.out_data)

	def generate_raw_data(self,path):
		raw_in = []
		raw_out = []
		for i in range(self.num_batches):
			for j in range(self.batch_size):
				raw_in.append( self.in_data[i][j] )
				raw_out.append( self.out_data[i][j] )

		raw_in = np.reshape(raw_in,[self.num_data,-1])
		raw_out = np.reshape(raw_out,[self.num_data,-1])

		np.savez(path,raw_in=raw_in, raw_out=raw_out)

	def load_data(self, data_path):
		file = np.load(data_path)
		in_data = file['in_data']
		out_data = file['out_data']

		self.in_data.clear()
		self.out_data.clear()

		self.out_data = out_data
		self.in_data = in_data

		#self.to_troch()

if __name__ == '__main__':
	d = data(num_batches=125,batch_size=4,num_out=200)
	#d.generate_test_data()

	#d.load_data('test_data.npz')
	d.new_batch_raw('raw_test.npz')

	#d = data(path='./raw_data.npz',batch_size=4)

