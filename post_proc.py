import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as inter
from matplotlib.pyplot import figure
import matplotlib.patches as patches

class plotter:
	def __init__(self):
		self.fig = figure(figsize=(8, 8), dpi=80, facecolor='w', edgecolor='k')
		self.ax = self.fig.add_subplot(1, 1, 1)
	
	def plot_interpolation(self,x,y,color_code='k',label=None):
		N = x.shape[0]
		coor_x = np.zeros(N+1)
		coor_x[0:N] = x
		coor_x[-1] = coor_x[0]
		coor_y = np.zeros(N+1)
		coor_y[0:N] = y
		coor_y[-1] = coor_y[0]

		t = np.linspace(0,N,N+1)
		fx = inter.splrep(t, coor_x,per=True)
		fy = inter.splrep(t, coor_y,per=True)
		t_new = np.linspace(0,N,1000)
		t_new = np.append(t_new, [t_new[0]])

		poly =[]
		poly_x = inter.splev(t_new, fx)
		poly.append(poly_x)
		poly_y = inter.splev(t_new, fy)
		poly.append(poly_y)

		plt.plot(poly_x, poly_y,color_code, label=label)

	def add_points(self,x,y):
		for i in range(0,x.size):
			plt.plot(x[i], y[i],'ko')

	def add_box(self,coor,w,l,label=None,color='r'):
		rect = patches.Rectangle(coor,w,l,linewidth=1,edgecolor=color,facecolor='none',label=label)
		self.ax.add_patch(rect)

	def add_circle(self,coor,r,label=None,color='k'):
		circ = patches.Circle(coor,r,linewidth=1,edgecolor=color,facecolor='none',label=label)
		self.ax.add_patch(circ)

	def add_line(self,x,y,label=None, color='k'):
		plt.plot(x,y,color=color,label=label)

	def add_label(self,xlabel,ylabel,fontsize=12):
		plt.xlabel(xlabel,fontsize=fontsize)
		plt.ylabel(ylabel,fontsize=fontsize)

	def add_legend(self,fontsize=12):
		plt.legend(fontsize=fontsize)

	def set_plot_lims(self,xlim):
		coors = np.linspace(xlim[0],xlim[1],6)
		self.ax.set_xticks(coors)
		self.ax.set_yticks(coors)

	def plot(self,x,y,color_code):
		plt.plot(x,y,color_code)

	def save_eps(self,path):
		plt.savefig(path, format='eps', dpi=300)

	def show(self):
		plt.show()

def plot_curve():
	data = np.load('results.npz')
	x = data['x']
	x_pred = data['x_pred']

	p = plotter()
	p.add_box([-1,-1],2,2,color='k')
	p.plot_interpolation(x[0:4],x[4:8],'r',label='exact')
	p.plot_interpolation(x_pred[0:4],x_pred[4:8],'b',label='predicted')
	p.set_plot_lims([-1.2,1.2])
	p.add_legend()
	p.show()

if __name__ == '__main__':
	plot_curve()
