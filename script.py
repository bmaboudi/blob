import numpy as np
#import gauss_hermit as gh
import high_fidelity as hf
import metropolis as metro
import nn_network

def sort_vectors(x,y):
	alpha = np.zeros(x.shape[0])
	for i in range(alpha.size):
		if(x[i]>0 and y[i]>0):
			alpha[i] = np.arctan(y[i]/x[i])
		elif(x[i]<0 and y[i]>0):
			alpha[i] = np.pi + np.arctan(y[i]/x[i])
		elif(x[i]<0 and y[i]<0):
			alpha[i] = np.pi + np.arctan(y[i]/x[i])
		else:
			alpha[i] = 2*np.pi + np.arctan(y[i]/x[i])

	return np.argsort(alpha)

def conv_pol(N,model_params):
	x = np.random.uniform(0,1,N-2)

	idx = np.random.permutation(N-2)
	cut_off = np.random.randint(N-3) + 1

	temp = np.sort(x[idx[0:cut_off]])
	x1 = np.zeros(temp.size+1)
	x1[0] = temp[0]
	for i in range(1,temp.size):
		x1[i] = temp[i]-temp[i-1]
	x1[-1] = 1 - temp[-1]

	temp = np.sort(-x[idx[cut_off:]])
	x2 = np.zeros(temp.size+1)
	x2[0] = -1 - temp[0]
	for i in range(1,temp.size):
		x2[i] = temp[i-1]-temp[i]
	x2[-1] = temp[-1]

	y = np.random.uniform(0,1,N-2)

	idx = np.random.permutation(N-2)
	cut_off = np.random.randint(N-3) + 1

	temp = np.sort(y[idx[0:cut_off]])
	y1 = np.zeros(temp.size+1)
	y1[0] = temp[0]
	for i in range(1,temp.size):
		y1[i] = temp[i]-temp[i-1]
	y1[-1] = 1 - temp[-1]

	temp = np.sort(-y[idx[cut_off:]])
	y2 = np.zeros(temp.size+1)
	y2[0] = -1 - temp[0]
	for i in range(1,temp.size):
		y2[i] = temp[i-1]-temp[i]
	y2[-1] = temp[-1]

	x = np.concatenate([x1,x2],axis=0)
	y = np.concatenate([y1,y2],axis=0)

	idx = np.random.permutation(x.size)
	x = x[idx]
	idx = np.random.permutation(y.size)
	y = y[idx]

	cos_ang = y/np.sqrt(x**2+y**2)
	idx = sort_vectors(x,y)
	#idx = np.argsort(alpha)
	x = x[idx]
	y = y[idx]

	p = np.zeros([N,2])
	for i in range(0,N-1):
		p[i+1,0] = p[i,0] + x[i]
		p[i+1,1] = p[i,1] + y[i]


	av = np.sum(p,axis = 0)/N
	p = p - av
	model_params['px'] = p[:,0]
	model_params['py'] = p[:,1]
	return model_params

high_fid = hf.high_fidelity(num_out=200)

model_params = {}
model_params = conv_pol(4,model_params)
#model_params['px'] = np.reshape([ 0.0337172, 0.57496496, -0.18364713, -0.42503504], [-1])
#model_params['py'] = np.reshape([-0.45829116, -0.33434094, 0.54170884, 0.25092327], [-1])

#modes = np.linspace(1,9,9)
#y_observed = []
#for mode in modes:
#	model_params['mode'] = [ mode ]
high_fid.set_params(model_params)
high_fid.assemble_rhs()
high_fid.assemble_bilinear_matrix()
y_observed = high_fid.solve()
#	y_observed.append( y )

#y_observed = np.reshape(y_observed,[-1])

mu = 0.0
sigma = 0.05

mtr = metro.Metropolis(y_observed, sigma)
eval_func = nn_network.network_eval('./model.pt')
mtr.set_eval_func(eval_func.eval_model)

print(model_params['px'])
print(model_params['py'])
#mtr.run_metro_conj()
#mtr.metro_alg()
mean = mtr.metro_multi_seed(1)

x = np.zeros(8)
x[0:4] = model_params['px']
x[4:8] = model_params['py']

mean = np.reshape(mean,[-1])
x_pred = mean

np.savez('results.npz',x=x,x_pred=x_pred)

#mean = mtr.compute_mean()
#print(mean)
#print(mtr.compute_var())

#np.savez('results.npz',y_observed=y_observed,y=y,y_pred=y_pred, x=x, x_pred=x_pred)

#mean_vec = np.zeros((1,8))
#var_vec = np.zeros((1,8))
#
#for i in range(0,1):
#    print('iteration :', (i+1))
#    mtr.metro_alg()
#    mean = mtr.compute_mean()
#    var = mtr.compute_var()
#    mean_vec[i,:] = mean
#    var_vec[i,:] = var
#
#
#file_name = 'result.npz'
#np.savez(file_name,params=model_params['blob_params'], mean_vec=mean_vec, var_vec=var_vec)
#
#print(mean)
#print(var)
