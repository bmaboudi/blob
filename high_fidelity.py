import numpy as np
from dolfin import *
from mshr import *
import scipy.interpolate as inter

set_log_level(50)

def output_indices(x):
	return (x[0]*x[0] + x[1]*x[1] <  2) and (x[0]*x[0] + x[1]*x[1] >  1 - 3e-3)

class random_field_blob(UserExpression):
	def set_params(self,model_params):
		self.px = model_params['px']
		self.py = model_params['py']
		self.N = self.px.shape[0]
		self.num_inter = 15
		self.height = 5.0
		self.compute_curve()

	def compute_curve(self):
		coor_x = np.zeros(self.N+1)
		coor_x[0:self.N] = self.px
		coor_x[-1] = coor_x[0]
		coor_y = np.zeros(self.N+1)
		coor_y[0:self.N] = self.py
		coor_y[-1] = coor_y[0]

		t = np.linspace(0,self.N,self.N+1)
		fx = inter.splrep(t, coor_x,per=True)
		fy = inter.splrep(t, coor_y,per=True)

		t_new = np.linspace(0,self.N,self.num_inter)
		t_new = np.append(t_new, [t_new[0]])

		self.poly =[]
		poly_x = inter.splev(t_new, fx)
		self.poly.append(poly_x)
		poly_y = inter.splev(t_new, fy)
		self.poly.append(poly_y)

	def inside(self, point):
		num_nodes = self.poly[0].size
		counter = 0
		v1 = np.zeros(2)
		v2 = np.zeros(2)
		for i in range(1,num_nodes):
			if( np.sign( point[1] - self.poly[1][i] ) * np.sign( point[1] - self.poly[1][i-1] ) < 0 ):
				if( self.poly[1][i-1] > self.poly[1][i] ):
					v1[0] = self.poly[0][i-1] - point[0]
					v1[1] = self.poly[1][i-1] - point[1]
					v2[0] = self.poly[0][i] - point[0]
					v2[1] = self.poly[1][i] - point[1]
				else:
					v1[0] = self.poly[0][i] - point[0]
					v1[1] = self.poly[1][i] - point[1]
					v2[0] = self.poly[0][i-1] - point[0]
					v2[1] = self.poly[1][i-1] - point[1]
				if( np.sign( v1[0]*v2[1] - v1[1]*v2[0] )<0 ):
					counter += 1

		return np.mod(counter, 2)

	def eval(self, values, x):
		if ( self.inside(x) == 0 ):
			values[0] = 1.0
		else:
			values[0] = self.height

class source_term(UserExpression):
	def eval(self, values, x):
		values[0] = 50*exp(-(pow(x[0] , 2) + pow(x[1], 2)) / 0.1)

def boundary(x):
	return x[0] < -1 + DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

class high_fidelity():
	def __init__(self, num_out = 100, num_modes=1):
		#domain = Rectangle(Point(-1,-1), Point(1,1))
		#self.mesh = generate_mesh(domain, 40)
		#mesh_file = File('mesh.xml')
		#mesh_file << self.mesh
		self.mesh = Mesh('mesh.xml')
		self.V = FunctionSpace(self.mesh, 'CG', 1)

		self.u0 = Constant(0.0)
		self.bc = DirichletBC(self.V, self.u0, boundary)

		self.u = TrialFunction(self.V)
		self.v = TestFunction(self.V)
		
		FEM_el = self.V.ufl_element()
		self.f = source_term(element = FEM_el)

		self.alpha = random_field_blob(element = FEM_el)
		self.num_out = num_out
		self.num_modes = num_modes

	def set_params(self, model_params):
		self.alpha.set_params(model_params)

	def assemble_rhs(self):
		self.L = self.f*self.v*dx
		#b = assemble(self.L)

	def assemble_bilinear_matrix(self):
		self.a = self.alpha*self.u.dx(0)*self.v.dx(0)*dx + self.alpha*self.u.dx(1)*self.v.dx(1)*dx

	def solve(self):
		self.sol = Function(self.V)
		solve(self.a == self.L, self.sol, bcs = self.bc)

		x = np.linspace(-1+1e-3,1-1e-3, int(self.num_out/2) )
		out_vec = []
		for i in range( len(x) ):
			out_vec.append( self.sol(x[i], 1-1e-3) )
		for i in range( len(x) ):
			out_vec.append( self.sol(x[i], -1+1e-3) )

		out_vec = np.reshape(out_vec,[-1])
		return out_vec

	def save_solution(self):
		file = File("solution.pvd")
		file << self.sol

	def save_random_field(self):
		alpha_func = interpolate(self.alpha, self.V)
		file = File('random_field.pvd')
		file << alpha_func

def sort_vectors(x,y):
	alpha = np.zeros(x.shape[0])
	for i in range(alpha.size):
		if(x[i]>0 and y[i]>0):
			alpha[i] = np.arctan(y[i]/x[i])
		elif(x[i]<0 and y[i]>0):
			alpha[i] = np.pi + np.arctan(y[i]/x[i])
		elif(x[i]<0 and y[i]<0):
			alpha[i] = np.pi + np.arctan(y[i]/x[i])
		else:
			alpha[i] = 2*np.pi + np.arctan(y[i]/x[i])

	return np.argsort(alpha)

def conv_pol(N,model_params):
	x = np.random.uniform(0,1,N-2)

	idx = np.random.permutation(N-2)
	cut_off = np.random.randint(N-3) + 1

	temp = np.sort(x[idx[0:cut_off]])
	x1 = np.zeros(temp.size+1)
	x1[0] = temp[0]
	for i in range(1,temp.size):
		x1[i] = temp[i]-temp[i-1]
	x1[-1] = 1 - temp[-1]

	temp = np.sort(-x[idx[cut_off:]])
	x2 = np.zeros(temp.size+1)
	x2[0] = -1 - temp[0]
	for i in range(1,temp.size):
		x2[i] = temp[i-1]-temp[i]
	x2[-1] = temp[-1]

	y = np.random.uniform(0,1,N-2)

	idx = np.random.permutation(N-2)
	cut_off = np.random.randint(N-3) + 1

	temp = np.sort(y[idx[0:cut_off]])
	y1 = np.zeros(temp.size+1)
	y1[0] = temp[0]
	for i in range(1,temp.size):
		y1[i] = temp[i]-temp[i-1]
	y1[-1] = 1 - temp[-1]

	temp = np.sort(-y[idx[cut_off:]])
	y2 = np.zeros(temp.size+1)
	y2[0] = -1 - temp[0]
	for i in range(1,temp.size):
		y2[i] = temp[i-1]-temp[i]
	y2[-1] = temp[-1]

	x = np.concatenate([x1,x2],axis=0)
	y = np.concatenate([y1,y2],axis=0)

	idx = np.random.permutation(x.size)
	x = x[idx]
	idx = np.random.permutation(y.size)
	y = y[idx]

	cos_ang = y/np.sqrt(x**2+y**2)
	idx = sort_vectors(x,y)
	#idx = np.argsort(alpha)
	x = x[idx]
	y = y[idx]

	p = np.zeros([N,2])
	for i in range(0,N-1):
		p[i+1,0] = p[i,0] + x[i]
		p[i+1,1] = p[i,1] + y[i]


	av = np.sum(p,axis = 0)/N
	p = p - av
	model_params['px'] = p[:,0]
	model_params['py'] = p[:,1]
	return model_params

if __name__=='__main__':
	hf = high_fidelity(num_out=200)

	model_params = {}
	model_params = conv_pol(4 ,model_params)
	#model_params['rf_exp'] = np.random.normal(0,1,2)
	#model_params['mode'] = [ np.random.uniform(0.0,10.0) ]

	hf.set_params(model_params)
	hf.assemble_rhs()
	hf.assemble_bilinear_matrix()
	hf.solve()
	hf.save_solution()
	hf.save_random_field()